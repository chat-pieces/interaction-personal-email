fs = require('fs').promises
imap = require 'imap-simple'
mailer = require 'nodemailer'
run = require '@aurium/run'
tmp = require 'tmp'

timeout = (secs, callback)-> setTimeout callback, secs*1000
interval = (secs, callback)-> setInterval callback, secs*1000


oneHour = 60*60*1000
oneHourAgo = new Date Date.now() - oneHour
twoHoursAgo = new Date Date.now() - 2*oneHour
yesterday = new Date Date.now() - 24*oneHour
twoDaysAgo = new Date Date.now() - 24*2*oneHour
treeDaysAgo = new Date Date.now() - 24*3*oneHour
daysAgo = (numDays)=> new Date Date.now() - numDays*24*oneHour
botMail = null

personalEmailClient = (bot, update)-> botMail.listen bot, update
personalEmailClient.init = (bot)->
    botMail.bot = bot
    do botMail.init

module.exports = (args)->
    botMail = new BotMail args
    personalEmailClient

class MailHeader
    constructor: (@headerData)->
    get: (key)->
        body = @headerData.body
        ( body[key] or body[key.toLowerCase()] or [] )[0]

meses = 'Janeiro Fevereiro Março Abril Maio Junho Julho Agosto Setembro Outubro Novembro Dezembro'.split ' '
mesesP = 'Jan Fev Mar Abr Mai Jun Jul Ago Set Out Nov Dez'.split ' '
humanDate = (d, long=false)->
    d = new Date d
    if long
        "#{d.getDate()} de #{meses[d.getMonth()]} de #{d.getFullYear()}"
    else
        "#{d.getDate()}/#{mesesP[d.getMonth()]}/#{d.getFullYear()}"

h = (str)-> str.replace(/&/gm, '&amp;').replace(/</gm, '&lt;').replace(/>/gm, '&gt;')

cleanMailTxt = (txt)->
    txt
    .replace /(\n\s*On [^\n]* wrote:).*/s, '$1\n(e-mail anterior)'
    .replace /(\n\s*Em [^\n]* escreveu:).*/s, '$1\n(e-mail anterior)'
    .replace /\n(>[^\n]*\n)+/sg, '\n(e-mail anterior)\n'

cleanToFromNames = (names)->
    names
    .replace /"?([^,"]+)"?\s*<[^>,]+>/gi, ' $1'
    .replace /<?([-_a-z0-9]+)@[^>,]+>?/gi, '$1'

getMailParts = (connection, parts, mail)->
    textParts = []
    attachments = []
    Promise.all([
        (parts
            .filter (part)-> part.type.toUpperCase() is 'TEXT'
            .map (part)->
                connection.getPartData(mail, part)
                .then (data)->
                    textParts.push {part..., data}
                .catch (err)->
                    textParts.push {
                        subtype: 'PLAIN'
                        data: "Can't load some e-mail part. #{err.message}\n\n#{err.stack}"
                    }
        )...,
        (parts
            .filter (part)-> part.disposition?.type.toUpperCase() is 'ATTACHMENT'
            .map (part)->
                connection.getPartData(mail, part)
                .then (data)->
                    attachments.push {
                        filename: part.disposition.params?.filename or 'unnamed',
                        data
                    }
        )...
    ])
    .then -> {textParts, attachments}

mkMailKeyboard = (mailUID, spanScore=0)->
    kbLine = [ { text: 'Del Mail', callback_data: 'delete-mail-'+mailUID } ]
    if spanScore > 0
        kbLine.push { text: "Spam (#{spanScore}%)", callback_data: 'junk-mail-'+mailUID }
        kbLine.push { text: "Not Junk", callback_data: 'non-junk-mail-'+mailUID }
    else
        kbLine.push { text: "Del this msg only", callback_data: 'delete-msg-'+mailUID }
    JSON.stringify inline_keyboard: [ kbLine ]


class BotMail

    constructor: ({@groupId, @user, @passwd, @host, @dataDir})->
        @histFile = "#{@dataDir}/mail-history-#{@user}@#{@host}"

    init: ->
        timeout 5, => do @findNewMailAndSendToUser
        interval 15*60, => do @findNewMailAndSendToUser

    listen: (bot, update)->
        return unless update._act?.chat?.id is @groupId
        replyMsgId = update._act.reply_to_message?.message_id
        if replyMsgId
            @findFirstMsgInHist 'msgId', replyMsgId
            .then (data)=> @sendReplyMail update._act, data
            .catch (err)=> @error err, 'grep hist file fail.'
        else if update._kind is 'callback_query'
            if match = update._act.data.match /^delete-mail-([0-9]+)/
                @deleteMail match[1], update._act
            if match = update._act.data.match /^delete-msg-([0-9]+)/
                @deleteMsg match[1], update._act
            if match = update._act.data.match /^junk-mail-([0-9]+)/
                @junkSpam match[1], update._act
            if match = update._act.data.match /^non-junk-mail-([0-9]+)/
                @nonJunk match[1], update._act

    log: (args...)-> @bot.logger.log args...
    error: (args...)-> @bot.logErrorAndMsgAdm args...

    connectIMAP: ->
        @log "Connecting IMAP to #{process.env.EMAIL_HOST}..."
        config =
            imap:
                user: @user
                password: @passwd
                host: @host
                port: 993
                authTimeout: 3000
                tls: true
                tlsOptions: rejectUnauthorized: false
            onmail: (numNewMail)=>
                @log 'New Email!', numNewMail
                #do @findNewMailAndSendToUser
            onclose: (err)=>
                err = message: 'No error.' unless err
                @bot.sendMessage 'IMAP Closed! '+err.message, @groupId
                timeout 30, =>
                    @connectIMAP().catch (err)=> @error err, 'IMAP Conn Fail'
        @conn = await imap.connect config

    openBox: ->
        @conn.openBox('INBOX')
        .catch => @connectIMAP().then => @conn.openBox('INBOX')

    findNewMailAndSendToUser: (retry=3)->
        searchCriteria = [
            'UNSEEN', '!DELETED'
            ['SINCE', daysAgo(1).toISOString()]
            ['!HEADER', 'x-spam-flag', 'YES']
            JSON.parse process.env.EMAIL_SEARCH
        ]
        @log 'IMAP searching for:', searchCriteria.join ' & '
        fetchOptions =
            bodies: ['HEADER', 'TEXT']
            markSeen: false
            struct: true
        (if @conn then do Promise.resolve else do @connectIMAP)
        .then => do @openBox
        .then => @conn.search searchCriteria, fetchOptions
        .then (results)=>
            @log "Found #{results.length} e-mails."
            for mail,i in results[0..20]
                do (mail)=> timeout i*20, => @sendMailToUser mail
        .catch (err)=>
            @error err, 'IMAP Search Fail'
            @bot.sendMessage 'IMAP Search Fail:\n'+err.message, @groupId
            if retry-- > 0
                timeout 10, => @connectIMAP().then => @findNewMailAndSendToUser retry

    findMsgsInHist: (key, value)->
        run 'egrep', "\"#{key}\"\\s*:\\s*#{value}\\b", @histFile
        .then (output)=>
            output = output.trim().split('\n').join ',\n'
            try
                JSON.parse "[#{output}]"
            catch parseError
                err = new Error "
                    The mail hist file #{@histFile} line \"#{output}\" is not a JSON.
                    \n#{parseError.message}
                "
                @bot.logger.error err
                throw err

    findFirstMsgInHist: (key, value)-> @findMsgsInHist(key, value).then (j)-> j[0]

    sendMailToUser: (mail)->
        debugLog = []
        debugLog.push "Init sendMailToUser"
        parts = imap.getParts mail.attributes.struct
        debugLog.push "Has #{parts.length} parts"
        #filenameLog = "/tmp/#{(new Date mail.attributes.date).toISOString()}--UID-#{mail.attributes.uid}.json"
        #fs.writeFile filenameLog, JSON.stringify(mail, null, '\t'), (err)=> @error err, "Write #{filenameLog}" if err
        date = new Date mail.attributes.date
        flags = mail.attributes.flags.filter (f)-> f isnt '\\Recent'
        mailUID = mail.attributes.uid
        header = new MailHeader mail.parts.filter((part)-> part.which is 'HEADER')[0]
        debugLog.push "Header: #{JSON.stringify header.headerData}"
        messageID = header.get 'Message-ID'
        listAddr = header.get 'List-Post'
        listName = header.get 'List-Id'
        replyTo = header.get 'Reply-To'
        from = header.get 'From'
        to = header.get('To') or 'hidden'
        cc = header.get('Cc') or ''
        remittee = (to + if cc then ", #{cc}" else '')
            .replace ///([^>,]*)?<?#{process.env.EMAIL_MATCH_ME}>?///i, ' me'
        subject = header.get 'Subject'
        spanScore = parseInt header.get 'X-Spam-Score'
        mailMetaData = {
            subject, messageID, mailUID,
            replyTo: (listAddr or replyTo or from).replace /mailto:/i, ''
        }
        bodyTxt = null
        msg = null
        msgSSML = null
        debugLog.push "Getting mail parts..."
        getMailParts @conn, parts, mail
        .then ({textParts, attachments})=>
            debugLog.push "Gotcha! #{textParts.length} text parts."
            #fs.writeFile filenameLog+".text-parts.json", JSON.stringify(textParts, null, '\t'),
            #    (err)=> @error err, 'Write text-parts.json' if err
            bodyTxt = textParts
                .filter (part)-> part.subtype.toUpperCase() is 'HTML'
                .map (part)-> part.data
                .join '\n<hr>\n'
            bodyTxt = @bot.cleanHTML bodyTxt if bodyTxt
            bodyTxt ?= h textParts
                .filter (part)-> part.subtype.toUpperCase() is 'PLAIN'
                .map (part)-> part.data
                .join '\n\n'
            bodyTxt ?= textParts[0]?.data or '[No Body]'
            bodyTxt = cleanMailTxt(bodyTxt).trim()
            @log 'Mail Body:' + JSON.stringify(bodyTxt.substr 0,200), bodyLength: bodyTxt.length
            msg = @bot.limitText """
                <b>#{h subject}</b>
                from: <code>#{h cleanToFromNames from}</code>
                to: <code>#{h cleanToFromNames listName or remittee}</code>
                #{humanDate date} #{h flags.join ' '}
                
                #{bodyTxt}
                """, isHTML:true
            msgSSML = """<speak>
                <h1>#{h subject}</h1>
                <s>de: #{h cleanToFromNames from},
                para: #{h cleanToFromNames listName or remittee}</s>
                <s>#{humanDate date, true}, #{h flags.join ' '}</s>
                <s>#{
                    bodyTxt
                    .replace /\n\s*•/g, '</s><s>'
                    .replace /\n\s*\n/g, '</s><s>'
                    .replace /https?:\/\/[-_\.\+a-z0-9\/]+/ig, '(url)'
                }</s>
                </speak>"""
            @log "ENVIANDO \"#{subject}\"..."
            ttsMsgId = null
            debugLog.push "Sending to user..."
            @sendTTSMailToUser msgSSML, subject
            .then (msgId)=> debugLog.push ttsMsgId = msgId
            .catch (err)=> @error err, 'Fail to send TTS mail to user.'
            .then => @sendTxtMailToUser msg, ttsMsgId, subject, mailUID, spanScore, attachments, mailMetaData
        .catch (err)=>
            @error err, "Unhandled error inside sendMailToUser\nSee /tmp/email-fail--#{subject}.*.txt"
            errTxt = err.message + '\n\n' + err.stack + '\n\n' + debugLog.join('\n\n')
            Promise.all [
                fs.writeFile "/tmp/email-fail--#{subject.replace /[^a-z0-9]/ig, '-'}.body.txt", bodyTxt
                fs.writeFile "/tmp/email-fail--#{subject.replace /[^a-z0-9]/ig, '-'}.msg.txt" , msg
                fs.writeFile "/tmp/email-fail--#{subject.replace /[^a-z0-9]/ig, '-'}.tts.txt" , msgSSML
                fs.writeFile "/tmp/email-fail--#{subject.replace /[^a-z0-9]/ig, '-'}.ERR.txt" , errTxt
            ]
        .catch @error.bind this

    sendTTSMailToUser: (msgSSML, subject)->
        new Promise (resolve, reject)=>
            tmp.dir postfix:'-email-TTS', (err, temp, cleanupCallback)=>
                do tmp.setGracefulCleanup
                return reject err if err
                tmpTxt = temp + '/email.ssml'
                tmpWav = temp + '/email.wav'
                tmpMp3 = temp + '/email.mp3'
                await fs.writeFile tmpTxt, msgSSML
                run 'espeak', '-vmb-br1', '-s140', '-m', '-f', tmpTxt, '-w', tmpWav, timeout: 30
                .then =>
                    run 'ffmpeg', '-y', '-i', tmpWav, tmpMp3, timeout: 30
                .then =>
                    @bot.sendVoice tmpMp3, @groupId, caption: subject, (err, response, result)=>
                        return reject err if err
                        resolve result.message_id
                .catch reject
                .then => do cleanupCallback

    sendTxtMailToUser: (msg, ttsMsgId, subject, mailUID, spanScore, attachments, mailMetaData)->
        new Promise (resolve, reject)=>
            params =
                parse_mode: 'HTML',
                reply_markup: mkMailKeyboard mailUID, spanScore
            params.reply_to_message_id = ttsMsgId if ttsMsgId
            mailMetaData.ttsMsgId = ttsMsgId
            @bot.sendMessage msg, @groupId, params, (err, response, result)=>
                    return reject err if err
                    @conn.addFlags mailUID, '\\Seen'
                    .then => @log "Mail \"#{subject}\" marked as Seen"
                    .catch (err)=>
                        @error err, 'IMAP addFlags Seen Fail'
                        @bot.sendMessage 'IMAP addFlags Seen Fail:\n'+err.message, @groupId
                    msgId = result.message_id
                    mailMetaData.msgId = msgId
                    fs.appendFile @histFile, JSON.stringify(mailMetaData) + '\n'
                    .catch (err)=> @error err, "e-Mail histFile append fail (#{@histFile})"
                    timeout .2, =>
                        for file in attachments
                            @bot.sendDocument file.data, @groupId,
                                file_name: file.filename, reply_to_message_id: msgId
                        do resolve

    sendReplyMail: (updateAct, mailMetaData)->
        transporter = mailer.createTransport
            host: @host
            port: 465
            secure: true # true for 465, false for other ports
            auth: user: @user, pass: @passwd
        mailOptions =
            from: @user
            to: mailMetaData.replyTo
            subject: "RE: #{mailMetaData.subject}"
            text: updateAct.text
        mailOptions.inReplyTo = mailMetaData.messageID if mailMetaData.messageID
        transporter.sendMail mailOptions, (err, info)=>
            if err
                @error err, "Send reply mail fail. (#{mailOptions.subject})"
                @bot.sendMessage 'Fail to send e-mail\n'+err.message, @groupId, reply_to_message_id: updateAct.message_id
            else
                @log info
                @bot.sendMessage 'e-Mail enviado!', @groupId, reply_to_message_id: updateAct.message_id

    deleteMailMsg: (message, mailMetaData)->
        @bot.deleteMessage message.chat.id, mailMetaData.ttsMsgId if mailMetaData.ttsMsgId
        @bot.deleteMessage message.chat.id, message.message_id

    junkSpam: (mailUID, updateAct)->
        message = updateAct.message
        @findFirstMsgInHist 'mailUID', mailUID
        .then (mail)=>
            #@bot.answerCallbackQuery updateAct.id, "Movendo \"#{mail.subject}\" para Junk."
            @conn.moveMessage mailUID, 'INBOX.Junk' #'INBOX.spam'
            .then =>
                @deleteMailMsg message, mail
            .catch (err)=>
                @bot.logger.error "Can't move \"#{mail.subject}\" to Junk.", err
                @bot.sendMessage "Can't move \"#{mail.subject}\" to Junk.\n"+err.message, @groupId
        .catch (err)=>
            @bot.sendMessage 'Can\'t find e-mail in history to Junk.\n'+err.message, @groupId

    nonJunk: (mailUID, updateAct)->
        message = updateAct.message
        @findFirstMsgInHist 'mailUID', mailUID
        .then (mail)=>
            #@bot.answerCallbackQuery updateAct.id, "Marcando \"#{mail.subject}\" como NonJunk."
            @conn.addFlags mailUID, 'NonJunk'
            .then =>
                @bot.editMessageReplyMarkup mkMailKeyboard(mailUID), message.chat.id, message.message_id
            .catch (err)=>
                @bot.logger.error "Can't mark \"#{mail.subject}\" as NonJunk.", err
        .catch (err)=>
            @bot.sendMessage 'Can\'t find e-mail in history to mark as NonJunk.\n'+err.message, @groupId

    deleteMail: (mailUID, updateAct)->
        message = updateAct.message
        @findFirstMsgInHist 'mailUID', mailUID
        .then (mail)=>
            #@bot.answerCallbackQuery updateAct.id, "\"#{mail.subject}\" foi marcado para deleção."
            @conn.addFlags mailUID, '\\Deleted'
            .then =>
                @deleteMailMsg message, mail
            .catch (err)=>
                @bot.logger.error "Can't delete e-mail \"#{mail.subject}\".", err
                @bot.sendMessage "Can't delete e-mail \"#{mail.subject}\".", @groupId
        .catch (err)=>
            @bot.sendMessage 'Can\'t find e-mail in history to Delete.\n'+err.message, @groupId

    deleteMsg: (mailUID, updateAct)->
        message = updateAct.message
        @findFirstMsgInHist 'mailUID', mailUID
        .then (mail)=>
            @deleteMailMsg message, mail
        .catch (err)=>
            @bot.logger.error "Can't delete message \"#{mail.subject}\".", err
            @bot.sendMessage "Can't delete message \"#{mail.subject}\".", @groupId

